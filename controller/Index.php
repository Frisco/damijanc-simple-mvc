<?php
/**
 * Created by PhpStorm.
 * User: crawler.energie-it
 * Date: 14.07.14
 * Time: 15:27
 */

namespace blog\controller;

use blog\logic\post;
use blog\data\Mapper\PostMapper;

class Index extends base {

    public function getPage()
    {

        //get the posts
        //get the number of comments

        $post = new post\Post();

        $result = $post->GetAllPosts();

        $mapper = new PostMapper();

        //convert DB data to class data
        $data = $mapper->getDomainData($result);

        foreach ($data as &$p) {
            $p->setNumberOfComments($post->GetNumberOfComments($p->getId()));
        }


        return parent::render('index.tpl.php', $data );
    }

} 