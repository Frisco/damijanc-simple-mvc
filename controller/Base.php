<?php
/**
 * Created by PhpStorm.
 * User: crawler.energie-it
 * Date: 14.07.14
 * Time: 15:26
 */

namespace blog\controller;


class Base {

    public function render($template, $vars=FALSE)
    {
        // Load up some global variables to put into the template's scope
        $GLOBALS['data'] = $vars;

        /*if ($vars !== FALSE)
            extract($vars);*/

        // Load up the template file's contents and save to the $_body variable
        // This variable will be utilized in base.html
        ob_start();
        include '../views/' . $template;

        $body = ob_get_contents();
        ob_end_clean();

        require_once('../views/' . '/base.tpl.php');
    }

} 