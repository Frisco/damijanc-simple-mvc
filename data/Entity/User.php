<?php
/**
 * Created by PhpStorm.
 * User: crawler.energie-it
 * Date: 14.07.14
 * Time: 10:49
 */

namespace blog\data\Entity;


class User extends Entity {

    private $email;
    private $password;

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

}