<?php
/**
 * Created by PhpStorm.
 * User: crawler.energie-it
 * Date: 14.07.14
 * Time: 10:49
 */

namespace blog\data\Entity;

include_once 'Entity.php';


class Post extends Entity {

    private $content;
    private $title;
    private $published;
    private $number_of_comments;

    /**
     * @param int $number_of_comments
     */
    public function setNumberOfComments($number_of_comments)
    {
        $this->number_of_comments = $number_of_comments;
    }

    /**
     * @return int
     */
    public function getNumberOfComments()
    {
        return $this->number_of_comments;
    }

    /**
     * @param string $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return string
     */
    public function getPublished()
    {
        return $this->published;
    }


    /**
     * @return string
     */
    public function getPublishedFormated()
    {
        if ( $this->published ) {
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $this->published);
            return $date->format('d.m.Y');
        }

    }

    /**
     * @param string $text
     */
    public function setContent($text)
    {
        $this->content = $text;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


} 