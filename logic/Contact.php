<?php
/**
 * Created by PhpStorm.
 * User: crawler.energie-it
 * Date: 14.07.14
 * Time: 14:56
 */

namespace blog\logic\contact;


use blog\data\Entity;
use blog\data;


class Contact {

    /**
     * @param Entity\Contact $contact
     * @return mixed
     */
    public function saveContact($contact) {
        if ( is_numeric($contact->getId())) {
            //update existing post
            $sql = sprintf("UPDATE `contact` SET ,`email`='%s',`content`='%s',`user_id`='%s' WHERE `id`=%d",
                $contact->getEmail(),$contact->getContent(),$contact->getUserId(),$contact->getId());

            $db = new data\DB();

            $db->BeginTransaction();
            $db->NonQuery($sql);
            $db->Commit();

        }else {
            //new record

            $sql = sprintf("INSERT INTO `contact`(`email`, `content`, `user_id`) VALUES ('%s','%s','%d')",
                $contact->getEmail(),$contact->getContent(),$contact->getUserId());

            $db = new data\DB();

            $db->BeginTransaction();
            $id = $db->NonQuery($sql);
            $db->Commit();

            $contact->setId($id);

        }

        return $contact;
    }

    /**
     * @param int $user_id
     * @return array
     */
    public function getAllContactsForUser($user_id) {

        $sql = sprintf('SELECT * FROM `contact` WHERE `user_id` = %d', $user_id);

        $db = new data\DB();

        $result = $db->Query($sql);

        return $result;

    }


    /**
     * @param int $id
     * @return array
     */
    public function getContact($id) {

        $sql = sprintf('SELECT * FROM `contact` WHERE `id` = %d', $id);

        $db = new data\DB();

        $result = $db->Query($sql);

        return $result;
    }


} 