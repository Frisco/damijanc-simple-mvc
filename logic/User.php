<?php
/**
 * Created by PhpStorm.
 * User: crawler.energie-it
 * Date: 14.07.14
 * Time: 11:25
 */

namespace blog\logic\user;

use blog\data;
use blog\data\Entity;


class User {

    public function Login($email, $password) {

        $sql = sprintf("select * from user where email = '%s' and password = '%s'", $email,$password);

        $db = new data\DB();

        $result = $db->Query($sql);

        if ($this->is_session_started() == FALSE) {
            session_start();
        }

        $_SESSION['user_id'] = $result[0]['id'];


    }

    public function Logout() {
        if ($this->is_session_started() == TRUE) {
            $_SESSION['user_id'] = '';
        }
    }

    public function IsUserLoggedIn() {
        if ($this->is_session_started() == TRUE) {
            if (is_numeric($_SESSION['user_id'])) {
                return $_SESSION['user_id'];
            }
        }
        return FALSE;
    }


    public function is_session_started()
    {
        if ( php_sapi_name() !== 'cli' ) {
            if ( version_compare(phpversion(), '5.4.0', '>=') ) {
                return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
            } else {
                return session_id() === '' ? FALSE : TRUE;
            }
        }
        return FALSE;
    }

} 