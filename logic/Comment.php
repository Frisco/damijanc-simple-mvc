<?php
namespace blog\logic\comment;


use blog\data\Entity;
use blog\data;



class Comment {

    public function GetAllCommentsForPost($post_id){

        $sql = sprintf("select * from comment where `post_id` = '%d'", $post_id);

        $db = new data\DB();

        $result = $db->Query($sql);

        return $result;
    }

    public function GetComment($id) {

        $sql = sprintf("select * from comment where id = '%d'", $id);

        $db = new data\DB();

        $result = $db->Query($sql);

        return $result;
    }

    /**
     * @param Entity\Comment $comment
     */
    public function SaveComment( $comment ) {

        if ( is_numeric($comment->getId())) {
            //update existing post
            $sql = sprintf("UPDATE `comment` SET `content`='%s',`post_id`=%d,`email`='%s',`url`='%s' WHERE `id`=%d",
                $comment->getContent(),$comment->getPostId(),$comment->getEmail(),$comment->getUrl(),$comment->getId());

            $db = new data\DB();

            $db->BeginTransaction();
            $db->NonQuery($sql);
            $db->Commit();

        }else {
            //new record

            $sql = sprintf("INSERT INTO `comment`(`content`, `post_id`, `email`, `url`) VALUES ('%s','%d','%s', '%s')",
                $comment->getContent(),$comment->getPostId(),$comment->getEmail(), $comment->getUrl());

            $db = new data\DB();

            $db->BeginTransaction();
            $id = $db->NonQuery($sql);
            $db->Commit();

            $comment->setId($id);

        }

        return $comment;

    }
} 