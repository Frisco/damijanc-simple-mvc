<?php
/**
 * Created by PhpStorm.
 * User: crawler.energie-it
 * Date: 14.07.14
 * Time: 11:27
 */

namespace blog\logic\post;

use blog\data\Entity;
use blog\data;


class Post {

    public function GetAllPostsForUser($user_id){

        $sql = sprintf("select * from post where `user_id` = '%d'", $user_id);

        $db = new data\DB();

        $result = $db->Query($sql);

        return $result;
    }

    public function GetAllPosts(){

        $sql = sprintf("select * from post order by created desc;");

        $db = new data\DB();

        $result = $db->Query($sql);

        return $result;
    }

    public function GetPost($id) {

        $sql = sprintf("select * from post where id = '%d'", $id);

        $db = new data\DB();

        $result = $db->Query($sql);

        return $result;
    }



    public function GetNumberOfComments($id) {

        $sql = sprintf("select count(*) as cnt from comment where post_id = '%d'", $id);

        $db = new data\DB();

        $result = $db->Query($sql);

        return $result[0]['cnt'];
    }

    /**
     * @param Entity\Post $post
     */
    public function SavePost( $post ) {

        if ( is_numeric($post->getId())) {
            //update existing post
            $sql = sprintf("UPDATE `post` SET `title`= '%s',`content`='%s',`published`='%s' WHERE `id` = %d",$post->getTitle(),$post->getContent(),$post->getPublished(),$post->getId());

            $db = new data\DB();

            $db->BeginTransaction();
            $db->NonQuery($sql);
            $db->Commit();

        }else {
            //new record

            $sql = sprintf("INSERT INTO `post`(`title`, `content`, `published`) VALUES ('%s','%s','%s')",$post->getTitle(),$post->getContent(),$post->getPublished());

            $db = new data\DB();

            $db->BeginTransaction();
            $id = $db->NonQuery($sql);
            $db->Commit();

            $post->setId($id);

        }

        return $post;

    }



} 